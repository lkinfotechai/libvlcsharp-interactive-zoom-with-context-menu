﻿using LibVLCSharp.Shared;
using System;
using System.Windows;

namespace LibVLCSharp.WPF.Sample
{
    public partial class MainWindow : Window
    {
        

        public MainWindow()
        {
            InitializeComponent();

            
        }

        void VideoView_Loaded(object sender, RoutedEventArgs e)
        {
            Core.Initialize();
            LibVLC _libVLC;
            MediaPlayer _mediaPlayer;
            _libVLC = new LibVLC();
            _mediaPlayer = new MediaPlayer(_libVLC);

            videoView.MediaPlayer = _mediaPlayer;

            _mediaPlayer.Play(new Media(_libVLC, new Uri("http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4")));
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            
           
            gridInsideView.Opacity = 0;
            Core.Initialize();
            LibVLC _libVLC;
            MediaPlayer _mediaPlayer;
            _libVLC = new LibVLC(true,new string[] { "--avcodec-hw=none", "--video-filter=magnify" });
            var media = new Media(_libVLC, new Uri("http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4"));
           // videoView.MediaPlayer.Dispose();
            videoView.MediaPlayer = new MediaPlayer(media);
            videoView.MediaPlayer.Play();
            media.Dispose();
           
        }
        private void MenuItem1_Click(object sender, RoutedEventArgs e)
        {


            VideoView_Loaded(null, null);

        }
    }
}